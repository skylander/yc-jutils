Author: yc <yanchuan@yanchuan.sg>  
Website: http://www.yanchuan.sg

This Java library consists of methods that I have needed/found useful when writing code for my research projects. It is not exhaustive nor well organized, but a collection of methods that are loosely organized into their own static classes. Feel free to use/copy/modify them as you like.

### Usage

Include the .jar file in your Java classpath when compiling/executing your Java code. See [here](http://stackoverflow.com/questions/1064481/newbie-question-how-to-include-jar-files-when-compiling) for more information.

### Documentation

Class and method descriptions are provided inline in the source code. Generated javadoc files can be found in the javadoc/ folder.

### Bugs

Report any bugs by sending me an email: yanchuan@yanchuan.sg